"use strict";

const mongoose = require('mongoose');

const {
  Schema
} = mongoose;

const jwt = require('jsonwebtoken');

const emailValidator = require('email-validator');

const bcrypt = require('bcrypt');

const config = require('../config');

const usersSchema = new Schema({
  username: {
    type: String,
    required: true,
    minlength: 3,
    unique: true
  },
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
    validate: {
      validator: emailValidator.validate,
      message: "Prière de saisir un email valide."
    }
  },
  password: {
    type: String,
    required: true,
    select: false
  },
  verified: {
    type: Boolean,
    default: false,
    select: false
  },
  pets: [{
    type: [mongoose.Schema.Types.ObjectId],
    ref: 'Pet'
  }]
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
});
usersSchema.methods = {
  authenticate: function (pass) {
    return bcrypt.compare(pass, this.password);
  },
  getToken: function () {
    return new Promise((resolve, reject) => {
      // const {password, pets, ...objet} = this._doc;
      // const objet = JSON.parse(JSON.stringify(sansPass));
      jwt.sign({
        id: this._id
      }, config.jwt_encryption, {
        expiresIn: `30d`
      }, (err, token) => {
        if (err) reject(err);else resolve(token);
      });
    });
  },
  adopter: function (pet) {
    this.pets.push(pet._id);
    return this.save();
  } // publierMe: function(popule = true) {
  //   const objet = _.clone(this);
  //   if(popule) {
  //     objet.pets = objet.pets.map((pet) => pet.publier());
  //   }
  //   return objet;
  // },
  //
  // publierAutres: function () {
  //   const objet = _.omit(this, ['pets', 'email', 'created_at', 'updated_at']);
  //
  //   return objet;
  // }

  /**
   *
   * @type {Model}
   */

};
module.exports = mongoose.model('User', usersSchema);
//# sourceMappingURL=users.js.map
"use strict";

const User = require('./schema/users');

const _ = require('underscore');

function publicUser(user) {
  const {
    password,
    ...publicUser
  } = user;
  return user;
}

function publicPet(pet) {
  const publicPet1 = _.omit(pet, ['nature', 'historique', 'etat']);

  publicPet1.etat = {
    cacas: pet.etat.cacas,
    ennui: Math.floor(pet.etat.ennui / 5),
    faim: Math.floor(pet.etat.ennui / 5),
    sommeil: Math.floor(pet.etat.sommeil / 5),
    siesteEnCours: pet.etat.sommeil !== null,
    mort: pet.etat.mort
  };
  return publicPet1;
}

module.exports.PublicUser = publicUser;
module.eports.PublicPet = publicPet;
//# sourceMappingURL=publicClasses.js.map
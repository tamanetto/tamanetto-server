"use strict";

const express = require('express');

const path = require('path');

const bodyParser = require('body-parser');

const logger = require('morgan');

const cors = require('cors');

const bcrypt = require('bcrypt');

const mongoose = require('mongoose');

const config = require('./config');

const indexRouter = require('./routes/index');

const usersRouter = require('./routes/users');

const petsRouter = require('./routes/pets');

const Schema = require('./schema/index');

const app = express();
mongoose.set('useCreateIndex', true);
mongoose.connect(`mongodb://${config.db_host}:${config.db_port}/${config.db_name}`, {
  user: config.db_user,
  pass: config.db_password,
  useNewUrlParser: true
}).then(() => {
  console.log("Connected to Mongodb.");
}).catch(err => {
  console.error("An error occurred during the mongodb connection : ", err);
});

const {
  User
} = require('./schema/index');

app.use(logger('dev'));
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(cors());
app.use('/', indexRouter);
app.use('/user', usersRouter);
app.use('/pet', petsRouter);
module.exports = app;
//# sourceMappingURL=app.js.map
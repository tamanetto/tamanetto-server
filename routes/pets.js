const express = require('express');
const router = express.Router();
const config = require('../config');
const checkToken = require('../util/checkToken');
const _ = require('underscore');
const PublicationMethods = require('../util/publicationMethods');

const {Pet, User} = require('../schema');

router.post('/create', async (req, res, next) => {
  // try {
  //   await checkToken(req.body.token)
  //   res.status(200);
  // }
  // catch (e) {
  //   console.error(e);
  // }
  if(!req.body.token || !req.body.name) {
    res.status(400).json({
      text: "La requête est invalide."
    });
  }
  else {
    checkToken(req.body.token).then((user) => {
      // console.log(user);
      const name = req.body.name.trim();
      if(name.length < 3) {
        res.status(400).json({
          text: "Le nom de l'animal est trop court."
        });
      }
      else {
        Pet.findOne({
          name,
          "etat.mort": false,
          "etat.cacas": 3,
        }, (err, result) => {
          if (err) {
            console.error(err);
            res.status(500).json({text: "Erreur interne."});
          } else if (result)
            res.status(400).json({text: "Un animal existe déjà avec ce nom."});
          else {
            const animal = new Pet({
              name,
              proprietaire: user._id,
            });
            animal.naissance();

            animal.save()
              .then(async newPet => {
                try {
                  await user.adopter(newPet);
                  // const publicPet = newPet.publier();

                  const publicPet = PublicationMethods.animalForOwner(newPet);
                  res.status(200).json(publicPet);
                } catch (e) {
                  console.error(e);
                  res.status(500).json({text: "Erreur interne."});
                }
              })
              .catch(err => {
                console.error(err);
                res.status(500).json({
                  text: "Erreur interne.",
                });
              })
          }
        });
      }
    }, (err) => {
      res.status(403).json({
        text: "Token invalide.",
      });
    });
  }
});

router.get('/', (req, res, next) => {
  if(!req.query.token || !req.query.id) {
    res.status(400).json({
      text: "Requête invalide.",
    });
  }
  else {
    checkToken(req.query.token)
      .then((user) => {
        Pet.findOne({
          _id: req.query.id,
          proprietaire: user.id,
        }, (err, result) => {
          if(err)
            res.status(500).json({text: "Erreur interne."});
          else if(!result)
            res.status(404).json({text: "Aucun animal trouvé."});
          else
            res.status(200).json(PublicationMethods.animalForOwner(result));
        });
      })
      .catch((err) => res.status(403).json({text: "Token invalide."}));
  }
});

module.exports = router;
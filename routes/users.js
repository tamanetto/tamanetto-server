const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const emailValidator = require('email-validator');
const config = require('../config');
const checkToken = require('../util/checkToken');
const PublicationMethods = require('../util/publicationMethods');

const {User} = require('../schema');

/* SIGNUP user. */
router.post('/signup', function(req, res, next) {
  if(!req.body.email || !req.body.password || !req.body.username) {
    res.status(400).json({
      text: "La requête est invalide.",
    });
  }
  else {
    const verif = new Promise(async (resolve, reject) => {
      const username = req.body.username.trim();
      const email = req.body.email.trim();
      if(!emailValidator.validate(email)) {
        reject({code: 400, text: "L'adresse mail est invalide."});
      }
      if(req.body.password.length < 3) {
        reject({code: 400, text: "Le mot de passe est trop court."});
      }
      if(username.length < 3) {
        reject({code: 400, text: "Le nom d'utilisateur est trop court."});
      }

      try {
        const pass = await bcrypt.hash(req.body.password, config.password_salt_rounds);

        const user = {
          username,
          password: pass,
          email,
        };

        const verifEmail = new Promise((localResolve, localReject) => {
          User.findOne({
            email: user.email,
          }, (err, result) => {
            if (err)
              localReject({code: 500, text: "Erreur interne."});
            else if (result) {
              localReject({code: 400, text: "Un utilisateur existe déjà avec cette adresse email."});
            } else {
              localResolve();
            }
          })
        })

        const verifUsername = new Promise((localResolve, localReject) => {
          User.findOne({
            username: user.username,
          }, (err, result) => {
            if (err)
              localReject({code: 500, text: "Erreur interne."});
            else if (result) {
              localReject({code: 400, text: "Un utilisateur existe déjà avec ce nom d'utilisateur."});
            } else {
              localResolve(user);
            }
          })
        });

        Promise.all([verifEmail, verifUsername])
          .then(() => {
            resolve(user);
          })
          .catch((err) => {
            reject(err);
          })
      }
      catch(e) {
        reject(e);
      }
    });

    verif.then((user) => {
      const _user = new User(user);
      _user.validate((err) => {
        if(err)
          res.status(400).json({
            text: err.message,
          });
      });

      _user.save()
        .then(async (newUser) => {
          res.status(200).json({
            text: "Success",
            token: await newUser.getToken(),
          });
        })
        .catch(err => {
          console.error(err);
          res.status(500).json({
            text: "Erreur interne.",
          })
        })
    }).catch((err) => {
      console.error(err);
      res.status(err.code).json({
        text: err.text,
      });
    })
  }
});

router.post('/login', (req, res, next) => {
  if(!req.body.username || !req.body.password) {
    res.status(400).json({text: "La requête est invalide."});
  }
  else {
    const username = req.body.username.trim();
    User.findOne({
      username,
    }).select('+password +verified').exec((err, user) => {
      if(err) {
        console.error(err);
        res.status(500).json({text: "Erreur interne."});
      }
      else if(!user) {
        res.status(401).json({text: "Vos identifiants sont invalides."});
      }
      else {
        user.authenticate(req.body.password).then(async valide => {
          if(valide) {
            res.status(200).json({
              text: "Success",
              token: await user.getToken(),
            })
          }
          else {
            res.status(401).json({text: "Vos identifiants sont invalides."});
          }
        }).catch(err => {
          console.error(err);
          res.status(500).json({text: "Erreur interne."});
        })
      }
    })
  }
});

// Returns all the informations about the connected user.
router.get('/', (req, res, next) => {
  if(!req.query.token) {
    res.status(400).json({
      text: "Requête invalide.",
    });
  }
  else {
    checkToken(req.query.token).then(
      (user) => {
        User
          .findOne({_id: user._id})
          .populate('pets')
          .exec(async (err, utilisateur) => {
            if(err) {
              console.error(err);
              res.status(500).json({text: "Erreur interne."});
            }
            else {
              // utilisateur.publierMe();
              try {
                res.status(200).json(await PublicationMethods.userForHimself(utilisateur));
              }
              catch(e) {
                console.error(e);
                res.status(500).json({text: "Erreur interne."});
              }
            }
        });
      },
      (err) => {
        res.status(403).json({text: "Token invalide."});
      }
    )
  }
});

module.exports = router;

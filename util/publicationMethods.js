const _ = require('underscore');

function userForHimself(userMongoose, populateStatus = 0) {
  return new Promise((resolve, reject) => {
    const callback = (err, result) => {
      if(err)
        reject(err);
      else {
        const publicUser = _.clone(result._doc);
        publicUser.pets = publicUser.pets.map(element => animalForOwner(element));
        resolve(publicUser);
      }
    }
    // Populate attendu et non-effectué. Donc on populate.
    if(populateStatus === 2) {
      userMongoose.populate('pets', callback);
    }
    else {
      callback(null, userMongoose);
    }
  });
}

function animalForOwner(animalMongoose) {
  const publicPet = _.omit(animalMongoose._doc, ['created_at', 'updated_at', 'nature', 'historique']);
  publicPet.etat = animalMongoose.getPublicState();

  return publicPet;
}

module.exports = {
  animalForOwner,
  userForHimself,
};
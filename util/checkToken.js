const config = require('../config');
/**
 * @type {Model}
 */
const {User} = require('../schema');
const jwt = require('jsonwebtoken');

module.exports = (token) => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, config.jwt_encryption, (err, infos) => {
      if(err)
        reject(err);
      else {
        User.findById(infos.id, '-password', (err, result) => {
          if(err)
            reject(err);
          else if(!result) {
            reject(new Error("No user with this id."));
          }
          else {
            resolve(result);
          }
        });
      }
    })
  });
};
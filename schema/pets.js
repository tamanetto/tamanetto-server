const mongoose = require('mongoose');
const {Schema} = mongoose;
const {ObjectId} = mongoose.Schema.Types;
const config = require('../config');
const _ = require('underscore');

const petSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  etat: {
    cacas: {
      required: true,
      default: 0,
      type: Number,
    },
    ennui: {
      required: true,
      default: 0,
      type: Number,
    },
    faim: {
      required: true,
      default: 0,
      type: Number,
    },
    sommeil: {
      default: 0,
      type: Number,
      required: true,
    },
    siesteEnCours: {
      type: Date,
      default: null,
    },
    degats: {
      type: Number,
      required: true,
      default: 0,
    },
    mort: {
      type: Boolean,
      required: true,
      default: false,
    },
    age: {
      type: Number,
      required: true,
      default: 0,
    }
  },
  historique: {
    repas: [Date],
    cacasDate: [Date],
    jeux: [Date],
    nettoyages: [Date],
    siestes: [{
      debut: Date,
      fin: Date,
    }],
  },
  nature: {
    gourmand: Number,
    transitRapide: Number,
    joyeux: Number,
    resistant: Number,
    dormeur: Number,
    energetique: Number,
    svelte: Number,
  },
  proprietaire: {
    type: ObjectId,
    ref: 'User',
    required: true,
  }
}, {timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  }});

petSchema.methods = {
  getPublicState: function() {
    const objet = {
      cacas: this.etat.cacas,
      ennui: Math.floor(this.etat.ennui/5),
      faim: Math.floor(this.etat.ennui/5),
      sommeil: Math.floor(this.etat.sommeil/5),
      siesteEnCours: this.etat.sommeil !== null,
      mort: this.etat.mort,
      age: this.etat.age,
    };

    return objet;
  },

  // publier: function(popule = false) {
  //   const objet = _.omit(this, ['nature', 'historique', 'created_at', 'updated_at']);
  //   // const objet = _.omit(this, '_doc.nature');
  //   this.publicState();
  //
  //   if(popule) {
  //     objet.proprietaire.publierMe(false);
  //   }
  //
  //   console.log(objet);
  //
  //   return objet;
  // },

  // getPublicObject: function(populateProprietaire = false) {
  //   return new Promise((resolve, reject) => {
  //     const mongooseObject = this;
  //     const afterPopulate = async (err, result) => {
  //       if(err)
  //         reject(err);
  //       else {
  //         const resultat = _.omit(result._doc, ['nature', 'historique', 'created_at', 'updated_at']);
  //         if(resultat.etat) {
  //           resultat.etat = mongooseObject.publicState();
  //         }
  //         if(populateProprietaire && resultat.proprietaire) {
  //           resultat.proprietaire = await resultat.proprietaire.getSemiPublicObject(false);
  //         }
  //
  //         resolve(resultat);
  //       }
  //     };
  //
  //     if (populateProprietaire) {
  //       mongooseObject.populate('proprietaire', afterPopulate);
  //     }
  //     else {
  //       afterPopulate(null, mongooseObject);
  //     }
  //   });
  // },

  naissance: function() {
    this.nature = {
      gourmand: _.random(100),
      transitRapide: _.random(100),
      joyeux: _.random(100),
      resistant: _.random(100),
      dormeur: _.random(100),
      energetique: _.random(100),
      svelte: _.random(100),
    }
  },
  nettoyage: function() {
    if(this.etat.cacas >= 1) {
      --this.etat.cacas;
      this.historique.nettoyages.add(new Date());
    }
  },
  dormir: function() {
    if(this.etat.sommeil === null) {
      this.etat.sommeil = new Date();
    }
  },
  reveil: function() {
    if(this.etat.sommeil !== null) {
      this.historique.siestes.push({
        debut: this.etat.sommeil,
        fin: new Date(),
      });
      this.etat.sommeil = null;
    }
  },
  jouer: function() {
    this.jeux.push(new Date());

  }
};

module.exports = mongoose.model('Pet', petSchema);
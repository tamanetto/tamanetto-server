const config = require('../config');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const saltRounds = 10;

/**
 *
 * @type {Model}
 */
exports.User = require('./users');
/**
 * @type {Model}
 */
exports.Pet = require('./pets');